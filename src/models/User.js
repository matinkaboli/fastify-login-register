import mongoose, { Schema } from 'mongoose';

export default mongoose.model('User', Schema({
  email: {
    trim: true,
    type: String,
    required: true,
    maxlenght: 300,
  },
  password: {
    trim: true,
    type: String,
    required: true,
    maxlenght: 500,
  },
}));
