export default {
  port: process.env.PORT || '8080',
  logger: process.env.NODE_ENV === 'development',
  dbAddress: process.env.DB || 'mongodb://127.0.0.1/fastify',
  hashKey: process.env.HASH_KEY || '*wtt_a4pv[ZZSd,+8C8346gfV',
  sessionKey: process.env.SESSION_KEY || 'f@6xg2ue=d->RM]PGaMp"vYvsM5abAH!7RU&',
};
