import hmac from 'Root/utils/hmac';
import User from 'Root/models/User';
import { hashKey } from 'Root/config';
import emailValidator from 'Root/utils/validators/email';

const schema = {
  schema: {
    body: {
      type: 'object',
      required: ['email', 'password'],
      properties: {
        email: {
          type: 'string',
        },
        password: {
          type: 'string',
          minLength: 8,
        },
      },
    },
  },
};

export default async (fastify, options) => {
  fastify.post('/register', schema, async (request, reply) => {
    const email = request.body.email.toLowerCase();
    const { password } = request.body;

    if (!emailValidator(email)) {
      return reply.code(400).send({
        statusCode: 400,
        error: 'Bad Request',
        message: 'body.email is not valid',
      });
    }

    const checkEmail = await User.findOne({ email });

    if (checkEmail) {
      return reply.code(409).send({
        statusCode: 409,
        error: 'conflict',
        message: 'body.email has already been taken',
      });
    }

    const user = new User({
      email,
      password: hmac(password, hashKey),
    });

    await user.save();

    request.session.user = { id: user._id };

    return reply.code(200).send({
      statusCode: 200,
      message: 'User is created.',
    });
  });
};
