import hmac from 'Root/utils/hmac';
import User from 'Root/models/User';
import { hashKey } from 'Root/config';
import emailValidator from 'Root/utils/validators/email';

const schema = {
  schema: {
    body: {
      type: 'object',
      required: ['email', 'password'],
      properties: {
        email: {
          type: 'string',
        },
        password: {
          type: 'string',
        },
      },
    },
  },
};

export default async (fastify, options) => {
  fastify.post('/login', schema, async (request, reply) => {
    const email = request.body.email.toLowerCase();
    const { password } = request.body;

    const checkEmail = await User.findOne({
      email,
      password: hmac(password, hashKey),
    });

    if (!checkEmail) {
      return reply.code(404).send({
        statusCode: 404,
        error: 'not found',
        message: 'User not found',
      });
    }

    request.session.user = { id: checkEmail._id };

    return reply.code(200).send({
      statusCode: 200,
      message: 'Credentials are correct',
    });
  });
};
