import 'babel-polyfill';
import fastify from 'fastify';
import process from 'process';
import mongoose from 'mongoose';
import helmet from 'fastify-helmet';
import fastifyCookie from 'fastify-cookie';
import fastifySession from 'fastify-session';

import loginRoute from './routers/users/login';
import registerRoute from './routers/users/register';

import {
  port,
  logger,
  dbAddress,
  sessionKey,
} from './config';

// DB
mongoose.Promise = global.Promise;

mongoose.connect(dbAddress, { useNewUrlParser: true });

mongoose.connection.on('error', () => {
  console.log('Error occured in database.');
  process.exit(1);
});

mongoose.connection.on('disconnected', () => {
  console.log('Database disconnected.');
  process.exit(1);
});

const app = fastify({ logger });

app.register(helmet);
app.register(fastifyCookie);
app.register(fastifySession, {
  secret: sessionKey,
  cookie: {
    maxAge: 1000 * 60 * 60 * 24,
  },
});

// Routers
app.register(loginRoute);
app.register(registerRoute);

const start = async () => {
  try {
    await app.listen(port);
  } catch (e) {
    app.log.error(e);
    process.exit(1);
  }
};

start();
